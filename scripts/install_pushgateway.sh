#!/bin/bash

# Exit on any error
set -e

# Define variables
PUSHGATEWAY_VERSION="1.8.0" # Change this to the latest version if needed
INSTALL_DIR="/opt/pushgateway"
SERVICE_FILE="/etc/systemd/system/pushgateway.service"

# Download Pushgateway
echo "Downloading Pushgateway..."
wget https://github.com/prometheus/pushgateway/releases/download/v${PUSHGATEWAY_VERSION}/pushgateway-${PUSHGATEWAY_VERSION}.linux-amd64.tar.gz

# Extract the tarball
echo "Extracting Pushgateway..."
tar -xzf pushgateway-${PUSHGATEWAY_VERSION}.linux-amd64.tar.gz

# Create installation directory
echo "Installing Pushgateway..."
sudo mkdir -p ${INSTALL_DIR}
sudo cp pushgateway-${PUSHGATEWAY_VERSION}.linux-amd64/pushgateway ${INSTALL_DIR}

# Create a systemd service file
echo "Creating systemd service file..."
cat <<EOF | sudo tee ${SERVICE_FILE}
[Unit]
Description=Pushgateway Service
After=network.target

[Service]
ExecStart=${INSTALL_DIR}/pushgateway
Restart=always

[Install]
WantedBy=multi-user.target
EOF

# Reload systemd daemon to recognize the new service
echo "Reloading systemd daemon..."
sudo systemctl daemon-reload

# Enable and start Pushgateway service
echo "Enabling and starting Pushgateway service..."
sudo systemctl enable pushgateway
sudo systemctl start pushgateway

# Clean up
echo "Cleaning up..."
rm -rf pushgateway-${PUSHGATEWAY_VERSION}.linux-amd64*
echo "Installation complete."