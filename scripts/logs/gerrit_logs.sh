#!/bin/bash
# /opt/collabnet/gerrit/logs

#variables
# Define color codes
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
BLUE='\033[0;34m'
NC='\033[0m'

SELECTED_DATE="$1"
OUTPUT_ERROR_FILE="$HOME/errorfile${SELECTED_DATE}.txt"
LOG_FILE_REPOSITORY="/opt/collabnet/gerrit/logs"
GERRIT_SYSTEM_LOG_FILE="$LOG_FILE_REPOSITORY/gerrit.system.log"
GERRIT_ARCHIVES_REPOSITORY="/opt/collabnet/teamforge/log-archive/gerrit"
SSHD_LOG="$LOG_FILE_REPOSITORY/sshd_log"

#functions 
isfileexists() {
    if [ -e "$1" ]; then
        echo -e "${GREEN}OK : $1 exists.${NC}"
    else
        echo -e "${RED}KO : $1 doesn't exist.${NC}"
        exit 1
    fi
}

isoutputfileexists() {
    if [ -e "$OUTPUT_ERROR_FILE" ]; then
            echo -e "${GREEN}OK : $OUTPUT_ERROR_FILE exists.${NC}"
    else
        echo -e "${RED}KO : $OUTPUT_ERROR_FILE don't exist.${NC}"
        echo -e "${YELLOW}INFO : $OUTPUT_ERROR_FILE is being created.${NC}"
        touch $OUTPUT_ERROR_FILE
        chmod 664 $OUTPUT_ERROR_FILE
        isfileexists "$OUTPUT_ERROR_FILE"
    fi
}

isdirectoryexists() {
    local directory="$1"
    if [ -d "$directory" ]; then
        echo -e "${GREEN}OK : Directory '$directory' exists.${NC}"
    else
        echo -e "${RED}KO : Directory '$directory' does not exist.${NC}"
        exit 1
    fi
}

check_date(){
    local input_date="$1"
    local today=$(date +'%Y-%m-%d')
    local input_seconds=$(date -d "$input_date" +%s)
    local today_seconds=$(date -d "$today" +%s)

    if ! [[ "$input_date" =~ ^[0-9]{4}-[0-9]{2}-[0-9]{2}$ ]]; then
        echo -e "${RED} KO: Invalid date format. Please use the format YYYY-MM-DD.${NC}"
        exit 1
    fi

    if [ "$input_seconds" -le "$today_seconds" ]; then
        echo -e "${GREEN}OK : Input date is today or in the past.${NC}"
    else
        echo -e "${RED}KO : Input date is in the future.${NC}"
        exit 
    fi
}

get_previous_day(){
    input_date="$1"
    if ! [[ "$input_date" =~ ^[0-9]{4}-[0-9]{2}-[0-9]{2}$ ]]; then
        echo -e "${RED}KO: get_previous_day Invalid date format. Please use the format YYYY-MM-DD.${NC}"
        return 1
    fi
    
    input_seconds=$(date -d "$input_date" +%s)
    one_day_seconds=$((60*60*24))
    previous_day_seconds=$((input_seconds - one_day_seconds))
    previous_day=$(date -d "@$previous_day_seconds" +'%Y-%m-%d')

    echo "$previous_day"
}

get_next_day(){
    input_date="$1"
    if ! [[ "$input_date" =~ ^[0-9]{4}-[0-9]{2}-[0-9]{2}$ ]]; then
        echo -e "${RED}KO: get_next_day Invalid date format. Please use the format YYYY-MM-DD.${NC}"
        return 1
    fi

    input_seconds=$(date -d "$input_date" +%s)
    one_day_seconds=$((60*60*24))
    next_day_seconds=$((input_seconds + one_day_seconds))
    next_day=$(date -d "@$next_day_seconds" +'%Y-%m-%d')

    echo "$next_day"
}

THEDAYBEFORE=$(get_previous_day $SELECTED_DATE)

is_today() {
    input_date="$1"
    if ! [[ "$input_date" =~ ^[0-9]{4}-[0-9]{2}-[0-9]{2}$ ]]; then
        echo -e "${RED}KO: is_today Invalid date format. Please use the format YYYY-MM-DD.${NC}"
        return 1
    fi
    today=$(date +'%Y-%m-%d')
    if [ "$input_date" = "$today" ]; then
        echo -e "${GREEN}OK : The input date $input_date is today.${NC}"
        return 0
    else
        echo -e "${BLUE}INFO : The input date $input_date is not today.${NC}"
        return 1
    fi
}

ssh_login(){
    date_argument="$1" 
    previous_day=$(get_previous_day $date_argument)
    total_errors=$(sudo grep "sshd-SshDaemon" "$SSHD_LOG" | grep "LOGIN" | awk -v input_date=$date_argument '$1 >= input_date { if (!seen[$0]++) { print $4 "\n" }}' | wc -l)
    total_errors_daybefore=$(sudo zgrep "sshd-SshDaemon" "$GERRIT_ARCHIVES_REPOSITORY/sshd_log-$date_argument.gz" | grep "LOGIN" |
awk -v input_date="$previous_day" '$1 >= input_date { if (!seen[$0]++) { print $4 "\n" }}' | wc -l)

    echo -e "${YELLOW}INFO : processing Top 10 of ssh authentication - $date_argument.${NC}" 
    echo "Top 10 of ssh login - $date_argument" >> "$OUTPUT_ERROR_FILE"
    
    if is_today "$date_argument"; then
        echo "Total : $total_errors" >> "$OUTPUT_ERROR_FILE"
        sudo grep "sshd-SshDaemon" "$SSHD_LOG" | grep "LOGIN" | \
        awk -v input_date=$date_argument '$1 >= input_date { if (!seen[$0]++) { print $4 "\n" }}' | \
        sort | uniq -c | sort -rn | head -n 10 | while read -r line; do \
            echo "$line"; 
        done >> "$OUTPUT_ERROR_FILE"
        echo "" >> "$OUTPUT_ERROR_FILE"
    else
        echo "Total : $total_errors_daybefore" >> "$OUTPUT_ERROR_FILE"
        sudo zgrep "sshd-SshDaemon" "$GERRIT_ARCHIVES_REPOSITORY/sshd_log-$date_argument.gz" | grep "LOGIN" | \
        awk -v input_date="$previous_day"  '$1 >= input_date { if (!seen[$0]++) { print $4 "\n" }}' | \
        sort | uniq -c | sort -rn | head -n 10 | while read -r line; do 
            echo "$line"; 
        done >> "$OUTPUT_ERROR_FILE"
        echo "" >> "$OUTPUT_ERROR_FILE"
    fi
}

ssh_auth_failure(){
    date_argument="$1" 
    previous_day=$(get_previous_day $date_argument)
    total_errors=$(sudo grep "sshd-SshDaemon" "$SSHD_LOG" | grep "LOGIN" | awk -v input_date="$date_argument" '$1 >= input_date { if (!seen[$0]++) { print $4 "\n" }}' | wc -l)
    total_errors_daybefore=$(sudo zgrep "sshd-SshDaemon" "$GERRIT_ARCHIVES_REPOSITORY/sshd_log-$date_argument.gz" | grep "LOGIN" | awk -v input_date="$date_argument"  '$1 >= input_date { if (!seen[$0]++) { print $4 "\n" }}' | wc -l)
    echo -e "${YELLOW}INFO : processing Top 10 of bad ssh authentication - $date_argument.${NC}" 
    echo "Top 10 of bad ssh authentication - $date_argument" >> "$OUTPUT_ERROR_FILE"
    
    if is_today "$date_argument"; then
        echo "Total : $total_errors" >> "$OUTPUT_ERROR_FILE"
        sudo grep "sshd-SshDaemon" "$SSHD_LOG" | grep "LOGIN" | \
        awk -v input_date="$date_argument" '$1 >= input_date { if (!seen[$0]++) { print $4 "\n" }}' | \
        sort | uniq -c | sort -rn | head -n 10 | while read -r line; do \
            echo "$line"; 
        done >> "$OUTPUT_ERROR_FILE"
        echo "" >> "$OUTPUT_ERROR_FILE"
    else
        echo "Total : $total_errors_daybefore" >> "$OUTPUT_ERROR_FILE"
        sudo zgrep "sshd-SshDaemon" "$GERRIT_ARCHIVES_REPOSITORY/sshd_log-$date_argument.gz" | grep "LOGIN" | \
        awk -v input_date="$previous_day"  '$1 >= input_date { if (!seen[$0]++) { print $4 "\n" }}' | \
        sort | uniq -c | sort -rn | head -n 10 | while read -r line; do 
            echo "$line"; 
        done >> "$OUTPUT_ERROR_FILE"
        echo "" >> "$OUTPUT_ERROR_FILE"
    fi
}

http_auth_failure(){
    date_argument="$1" 
    previous_day=$(get_previous_day $date_argument)
    total_errors=$(sudo grep -ri "error authenticating user" "$GERRIT_SYSTEM_LOG_FILE" | awk -v input_date="$date_argument" '$1 >= input_date { if (!seen[$0]++) { print $4, $5, $6, $7, $8 "\n" }}' | wc -l)
    total_errors_daybefore=$(sudo zgrep "error authenticating user" "$GERRIT_ARCHIVES_REPOSITORY/gerrit.system.log-$date_argument.gz" | awk -v input_date="$previous_day" '$1 >= input_date { if (!seen[$0]++) { print $4, $5, $6, $7, $8 "\n" }}' | wc -l)
    echo -e "${YELLOW}INFO : processing Top 10 of bad http authentication - $date_argument.${NC}" 
    echo "Top 10 of bad http authentication - $date_argument" >> "$OUTPUT_ERROR_FILE"
    
    if is_today "$date_argument"; then
        echo "Total : $total_errors" >> "$OUTPUT_ERROR_FILE"
        sudo grep -ri "error authenticating user" "$GERRIT_SYSTEM_LOG_FILE" | \
        awk -v input_date="$date_argument" '$1 >= input_date { if (!seen[$0]++) { print $4, $5, $6, $7, $8 "\n" }}' | \
        sort | uniq -c | sort -nr | head -n 10 | while read -r line; do
            echo "$line"
        done >> "$OUTPUT_ERROR_FILE"
        echo "" >> "$OUTPUT_ERROR_FILE"
    else
        echo "Total : $total_errors_daybefore" >> "$OUTPUT_ERROR_FILE"
        sudo zgrep "error authenticating user" "$GERRIT_ARCHIVES_REPOSITORY/gerrit.system.log-$date_argument.gz" | \
        awk -v input_date="$previous_day" '$1 >= input_date { if (!seen[$0]++) { print $4, $5, $6, $7, $8 "\n" }}' | \
        sort | uniq -c | sort -nr | head -n 10 | while read -r line; do 
            echo "$line"; 
        done >> "$OUTPUT_ERROR_FILE"
        echo "" >> "$OUTPUT_ERROR_FILE"
    fi
}

concurrent_user_limit_reached(){
    date_argument="$1"
    previous_day=$(get_previous_day $date_argument)
    total_errors=$(sudo grep -ri "Max connection count for user" "$GERRIT_SYSTEM_LOG_FILE" | awk -v input_date="$date_argument" '$1 >= input_date { if (!seen[$0]++) { print $4, $5, $6, $7, $8, $9 "\n" }}' | wc -l)
    total_errors_daybefore=$(sudo zgrep "Max connection count for user" "$GERRIT_ARCHIVES_REPOSITORY/gerrit.system.log-$date_argument.gz" | awk -v input_date="$previous_day" '$1 >= input_date { if (!seen[$0]++) { print $4, $5, $6, $7, $8, $9 "\n" }}' | wc -l)
    echo -e "${YELLOW}INFO : processing Top 10 of max concurrent user limit reached - ${date_argument}.${NC}"
    echo "Top 10 of max concurrent user limit reached - ${date_argument}" >> "$OUTPUT_ERROR_FILE"
    
    if is_today "$date_argument"; then
        echo "Total : $total_errors" >> "$OUTPUT_ERROR_FILE"
        sudo grep -ri "Max connection count for user" "$GERRIT_SYSTEM_LOG_FILE" | \
        awk -v input_date="$date_argument" '$1 >= input_date { if (!seen[$0]++) { print $4, $5, $6, $7, $8, $9 "\n" }}' | \
        sort | uniq -c | sort -nr | head -n 10 | while read -r line; do
            echo "$line"
        done >> "$OUTPUT_ERROR_FILE"
        echo "" >> "$OUTPUT_ERROR_FILE"
    else
        echo "Total : $total_errors_daybefore" >> "$OUTPUT_ERROR_FILE"
        sudo zgrep "Max connection count for user" "$GERRIT_ARCHIVES_REPOSITORY/gerrit.system.log-$date_argument.gz" | \
        awk -v input_date="$previous_day" '$1 >= input_date { if (!seen[$0]++) { print $4, $5, $6, $7, $8, $9 "\n" }}' | \
        sort | uniq -c | sort -nr | head -n 10 | while read -r line; do 
            echo "$line"; 
        done >> "$OUTPUT_ERROR_FILE"
        echo "" >> "$OUTPUT_ERROR_FILE"
    fi
}

synchronisation_trouble_gerrit_teamforge(){
    date_argument="$1"
    previous_day=$(get_previous_day $date_argument)
    total_errors=$(sudo grep -ri "Cannot get user roles and groups for user" "$GERRIT_SYSTEM_LOG_FILE" | awk -v input_date="$date_argument" '$1 >= input_date { if ($4 == "Cannot" && !seen[$0]++) { print $4, $5, $6, $7, $8, $9, $10, $11, $12 "\n" }}' | wc -l)
    total_errors_daybefore=$(sudo zgrep "Cannot get user roles and groups for user" "$GERRIT_ARCHIVES_REPOSITORY/gerrit.system.log-$date_argument.gz" | awk -v input_date="$previous_day" '$1 >= input_date { if ($4 == "Cannot" && !seen[$0]++) { print $4, $5, $6, $7, $8, $9, $10, $11, $12 "\n" }}' | wc -l)
    echo -e "${YELLOW}INFO : processing Top 10 bad sync between Teamforge and Gerrit - ${date_argument}.${NC}"
    echo "Top 10 bad sync between Teamforge and Gerrit - ${date_argument}" >> "$OUTPUT_ERROR_FILE"
    
    if is_today "$date_argument"; then
        echo "Total : $total_errors" >> "$OUTPUT_ERROR_FILE"
        sudo grep -ri "Cannot get user roles and groups for user" "$GERRIT_SYSTEM_LOG_FILE" | \
        awk -v input_date="$date_argument" '$1 >= input_date { if ($4 == "Cannot" && !seen[$0]++) { print $4, $5, $6, $7, $8, $9, $10, $11, $12 "\n" }}' | \
        sort | uniq -c | sort -nr | head -n 10 | while read -r line; do
            echo "$line"
        done >> "$OUTPUT_ERROR_FILE"
        echo "" >> "$OUTPUT_ERROR_FILE"
    else
        echo "Total : $total_errors_daybefore" >> "$OUTPUT_ERROR_FILE"
        sudo zgrep "Cannot get user roles and groups for user" "$GERRIT_ARCHIVES_REPOSITORY/gerrit.system.log-$date_argument.gz" | \
        awk -v input_date="$previous_day" '$1 >= input_date { if ($4 == "Cannot" && !seen[$0]++) { print $4, $5, $6, $7, $8, $9, $10, $11, $12 "\n" }}' | \
        sort | uniq -c | sort -nr | head -n 10 | while read -r line; do 
            echo "$line"; 
        done >> "$OUTPUT_ERROR_FILE"
        echo "" >> "$OUTPUT_ERROR_FILE"
    fi
}

ssh_login_per_repo(){
    date_argument="$1"
    previous_day=$(get_previous_day $date_argument)
    total_errors=$(sudo grep -ri "git-upload-pack" "$GERRIT_SYSTEM_LOG_FILE" | awk -v input_date="$date_argument" '$1 >= input_date { if (!seen[$0]++) { print $4, $5, $6 "\n" }}' | wc -l)
    total_errors_daybefore=$(sudo zgrep "git-upload-pack" "$GERRIT_ARCHIVES_REPOSITORY/gerrit.system.log-$date_argument.gz" | awk -v input_date="$previous_day" '$1 >= input_date { if (!seen[$0]++) { print $4, $5, $6 "\n" }}' | wc -l)
    
    echo -e "${YELLOW}INFO : processing Top 10 of ssh login per repo - ${date_argument}.${NC}"
    echo "Top 10 of ssh login per repo - ${date_argument}" >> "$OUTPUT_ERROR_FILE"
    
    if is_today "$date_argument"; then
        echo "Total : $total_errors" >> "$OUTPUT_ERROR_FILE"
        sudo grep -ri "git-upload-pack" "$GERRIT_SYSTEM_LOG_FILE" | \
        awk -v input_date="$date_argument" '$1 >= input_date { if (!seen[$0]++) { print $4, $5, $6 "\n" }}' | \
        sort | uniq -c | sort -nr | head -n 10 | cut -d '|' -f1 | while read -r line; do
            echo "$line"
        done >> "$OUTPUT_ERROR_FILE"
        echo "" >> "$OUTPUT_ERROR_FILE"
    else
        echo "Total : $total_errors_daybefore" >> "$OUTPUT_ERROR_FILE"
        sudo zgrep "git-upload-pack" "$GERRIT_ARCHIVES_REPOSITORY/gerrit.system.log-$date_argument.gz" | \
        awk -v input_date="$previous_day" '$1 >= input_date { if (!seen[$0]++) { print $4, $5, $6 "\n" }}' | \
        sort | uniq -c | sort -nr | head -n 10 | cut -d '|' -f1 | while read -r line; do
            echo "$line"
        done >> "$OUTPUT_ERROR_FILE"
        echo "" >> "$OUTPUT_ERROR_FILE"
    fi
}

permission_issue() {
    error_number="$1"
    echo -e "${YELLOW}INFO : Top 10 error $error_number.${NC}"
    echo "Permission issues with error $error_number" >> "$OUTPUT_ERROR_FILE"
    
    sudo grep -ri "HttpErrorCode:$error_number\|($error_number)" "$GERRIT_SYSTEM_LOG_FILE" | \
    sort | uniq -c | sort -nr | head -n 10 | while read -r line; do
        echo "$line"
    done >> "$OUTPUT_ERROR_FILE"
    echo "" >> "$OUTPUT_ERROR_FILE"
}

check_one_argument() {
    if [ $# -eq 1 ]; then
        echo -e"${GREEN}One argument provided: $1.${NC}"
        check_date "$SELECTED_DATE"
    else
        echo -e"${RED}Incorrect number of arguments. Please provide exactly one argument.${NC}"
        exit 1
    fi
}

#pre-checks
check_one_argument
isdirectoryexists "$LOG_FILE_REPOSITORY"
isfileexists "$GERRIT_SYSTEM_LOG_FILE"
isdirectoryexists "$GERRIT_ARCHIVES_REPOSITORY"
isfileexists "$SSHD_LOG"
isoutputfileexists "$OUTPUT_ERROR_FILE"

#Top 10 of ssh login - Today
ssh_login "$SELECTED_DATE"
#Top 10 of ssh login - Yesterday
ssh_login "$THEDAYBEFORE"
#Top 10 of bad ssh authentication - Today
ssh_auth_failure "$SELECTED_DATE"
#Top 10 of bad ssh authentication - Yesterday
ssh_auth_failure "$THEDAYBEFORE"
#Top 10 of bad http authentication - Today
http_auth_failure "$SELECTED_DATE"
#Top 10 of bad http authentication - Yesterday
http_auth_failure "$THEDAYBEFORE"
#Top 10 of max concurrent user limit reached - Today
concurrent_user_limit_reached "$SELECTED_DATE"
#Top 10 of max concurrent user limit reached - Yesterday
concurrent_user_limit_reached "$THEDAYBEFORE"
#Top 10 of bad sync between Teamforge and Gerrit - Today
synchronisation_trouble_gerrit_teamforge "$SELECTED_DATE"
#Top 10 of bad sync between Teamforge and Gerrit - Yesterday
synchronisation_trouble_gerrit_teamforge "$THEDAYBEFORE"
#Top 10 of ssh login per repo - Today
ssh_login_per_repo "$SELECTED_DATE"
#Top 10 of ssh login per repo - Yesterday
ssh_login_per_repo "$THEDAYBEFORE"
# Top 10 Permssion issues with error 401
permission_issue "401"
# Top 10 Auth issues with error 403
permission_issue "403"
# Top 10 issues with error 500
permission_issue "500"
