#!/bin/bash

#variables
# Define color codes
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
BLUE='\033[0;34m'
NC='\033[0m'

CURRENT_DATE=$(date "+%d%m%Y_%H%M")

#all log files to survey
STRING_TO_GREP="error"
ADDONS_DIRECTORY="/opt/collabnet/teamforge/log/add-ons/"
OUTPUT_DIRECTORY="/data/logscrapper"
OUTPUT_LOGS_DIRECTORY="$OUTPUT_DIRECTORY/logs"
OUTPUT_ERROR_FILE="$OUTPUT_LOGS_DIRECTORY/errorfile${CURRENT_DATE}.txt"
APPS_VAMESSAGES_LOG_FILES="/opt/collabnet/teamforge/log/apps/vamessages.log"
APPS_SERVER_LOG_FILES="/opt/collabnet/teamforge/log/apps/server.log"
APPS_JBOSS_CONSOLE_LOG_FILES="/opt/collabnet/teamforge/log/apps/jboss_console.log"
APPS_CTEVENTS_LOG_FILES="/opt/collabnet/teamforge/log/apps/ctfevents.log"
CLISERVER_LOG_FILES="/opt/collabnet/teamforge/log/cliServer/cli-cron.log"
ETL_LOG_FILES="/opt/collabnet/teamforge/log/etl/etl.log"
HTTPD_SSL_ERROR_LOG_FILES="/opt/collabnet/teamforge/log/httpd/ssl_error_log"
PHOENIX_DEFAULT_SEARCH_LOG_FILES="/opt/collabnet/teamforge/log/phoenix/default-search.log"
PHOENIX_MAILET_LOG_FILES="/opt/collabnet/teamforge/log/phoenix/mailet.log"
PHOENIX_PHOENIX_CONSOLE_LOG_FILES="/opt/collabnet/teamforge/log/phoenix/phoenix.console"
PHOENIX_SMTPSERVER_LOG_FILES="/opt/collabnet/teamforge/log/phoenix/smtpserver.log"
WEBR_LOG_FILES="/opt/collabnet/teamforge/log/webr/service.log"
ADDONS_LOG_FILES=()   
for file in "$ADDONS_DIRECTORY"*; do
    if [ -s "$file" ] && [ -f "$file" ]; then
        ADDONS_LOG_FILES+=("$file")
    fi
done
echo -e "${BLUE}INFO : add-ons ${ADDONS_LOG_FILES[@]}.${NC}"

LOG_FILES_LIST=(
    "$APPS_VAMESSAGES_LOG_FILES"
    "$APPS_SERVER_LOG_FILES"
    "$APPS_JBOSS_CONSOLE_LOG_FILES"
    "$APPS_CTEVENTS_LOG_FILES"
    "$CLISERVER_LOG_FILES"
    "$ETL_LOG_FILES"
    "$HTTPD_SSL_ERROR_LOG_FILES"
    "$PHOENIX_DEFAULT_SEARCH_LOG_FILES"
    "$PHOENIX_MAILET_LOG_FILES"
    "$PHOENIX_PHOENIX_CONSOLE_LOG_FILES"
    "$PHOENIX_SMTPSERVER_LOG_FILES"
    "$WEBR_LOG_FILES"
    "${ADDONS_LOG_FILES[@]}"
)

GROUP1=("$WEBR_LOG_FILES")
GROUP2=("$APPS_VAMESSAGES_LOG_FILES")
GROUP3=("$HTTPD_SSL_ERROR_LOG_FILES")
GROUP4=("$APPS_SERVER_LOG_FILES" "$APPS_JBOSS_CONSOLE_LOG_FILES" "$APPS_CTEVENTS_LOG_FILES")
GROUP5=("$CLISERVER_LOG_FILES" "$ETL_LOG_FILES")
GROUP6=("$PHOENIX_DEFAULT_SEARCH_LOG_FILES" "$PHOENIX_MAILET_LOG_FILES" "$PHOENIX_SMTPSERVER_LOG_FILES")
GROUP7=("$PHOENIX_PHOENIX_CONSOLE_LOG_FILES")

COMMAND1="grep -ri \$STRING_TO_GREP \$file_path | awk '\$0 != \"\" { if (!seen[\$0]++){print \$4, \$5, \$6, \$7}}' | sort | uniq -c | sort -nr | head -n 10"
COMMAND2="grep -ri \$STRING_TO_GREP \$file_path | awk '\$0 != \"\" { if (!seen[\$0]++) print \$3, \$4, \$5, \$6, \$7 }' | sort | uniq -c | sort -nr | head -n 10"
COMMAND3="grep -ri \$STRING_TO_GREP \$file_path | awk '\$0 != \"\" { if (!seen[\$0]++){print \$13, \$14}}' | sort | uniq -c | sort -nr | head -n 10"
COMMAND4="grep -ri \$STRING_TO_GREP \$file_path | awk '\$0 != \"\" { if (!seen[\$0]++){print \$3, \$4, \$5}}' | sort | uniq -c | sort -nr | head -n 10"
COMMAND5="grep -ri \$STRING_TO_GREP \$file_path | awk '\$0 != \"\" { if (!seen[\$0]++){print \$3, \$4, \$5, \$6, \$7, \$8, \$9}}' | sort | uniq -c | sort -nr | head -n 10"
COMMAND6="grep -ri \$STRING_TO_GREP \$file_path | sort | uniq -c | sort -nr | head -n 10"
COMMAND7="grep -ri \$STRING_TO_GREP \$file_path | awk '\$0 != \"\" { if (!seen[\$0]++) print \$7, \$8, \$9, \$10 }' | sort | uniq -c | sort -nr | head -n 10"

#functions 
isfileexists() {
    if [ -e "$1" ]; then
        echo -e "${GREEN}OK : $1 exists.${NC}"
    else
        echo -e "${YELLOW}INFO : $1 don't exist yet.${NC}"
        exit 1
    fi
}

isfileaccessible() {
    local file="$1"
    if [ -r "$file" ]; then
        echo -e "${GREEN}OK : $file is accessible.${NC}"
    else
        echo -e "${RED}Error : $file is not accessible or permission denied.${NC}"
        exit 1
    fi
}

check_and_create_directory() {
    local DIRECTORY="$1"

    if [ ! -d "$DIRECTORY" ]; then
        mkdir -p "$DIRECTORY"
        echo -e "${GREEN}Directory '$DIRECTORY' created.${NC}"
    else
        echo -e "${BLUE}Directory '$DIRECTORY' already exists.${NC}"
    fi
}

delete_old_log_files() {
    local OLD_OUTPUT_FILES=$(find "$OUTPUT_LOGS_DIRECTORY" -maxdepth 1 -type f -name "errorfile*.txt" -mtime +31)
    if [ -n "$OLD_OUTPUT_FILES" ]; then
        echo -e "${YELLOW}Deleting log files older than 31 days...${NC}"
        echo "$OLD_OUTPUT_FILES" | xargs rm -f
        echo -e "${GREEN}OK : Old log files deleted.${NC}"
    else
        echo -e "${BLUE}INFO : No log files older than 31 days found.${NC}"
    fi
}

count_and_display_errors() {
    local command_to_run="$1"
    shift
    local group_name="$@"
    error_count=0
    
    for file_path in ${group_name[@]}; do
        error_count=$(eval "$command_to_run" | wc -l)
        if isfileaccessible "$file_path"; then
            echo -e "${YELLOW}INFO : processing $file_path.${NC}"  
            echo "******************************************************************************************************" >> "$OUTPUT_ERROR_FILE"
            if [ "$error_count" -eq 0 ] || [ "$error_count" -eq 1 ]; then
                echo -e "$file_path" : $error_count ERROR >> "$OUTPUT_ERROR_FILE"
            else
                echo -e "$file_path" : $error_count ERRORS >> "$OUTPUT_ERROR_FILE"
            fi
            echo "******************************************************************************************************" >> "$OUTPUT_ERROR_FILE"

            if [ $error_count -eq 0 ]; then
                    echo -e "${GREEN}OK : No error found in $file_path${NC}"
                    echo >> "$OUTPUT_ERROR_FILE"
                    echo "No error found in $file_path" >> "$OUTPUT_ERROR_FILE"
                    echo >> "$OUTPUT_ERROR_FILE"
            else
                    echo >> "$OUTPUT_ERROR_FILE"
                    eval "$command_to_run" >> "$OUTPUT_ERROR_FILE"
                    echo >> "$OUTPUT_ERROR_FILE"
            fi
        fi
    done
}

failed_connection_errors(){
    local TOTAL_TODAY_UNIQ_USER_CONNECTION_FAILURE=$(journalctl -u sshd --since "-1d" | grep "failed" | awk '$0 != "" { if (!seen[$0]++) print $1, $2, $4, $7, $14, $15 }' | uniq -c | sort -nr | head -n 10 | wc -l)
    local TOTAL_TODAY_CONNECTION_FAILURE_BY_USER=$(journalctl -u sshd --since "-1d" | grep "failed" | awk '$0 != "" { if (!seen[$0]++) print $1, $2, $4, $7, $14, $15 }' | uniq -c | sort -nr | head -n 10)
    echo "" >> "$OUTPUT_ERROR_FILE"

    if [ $TOTAL_TODAY_UNIQ_USER_CONNECTION_FAILURE -gt 0 ]; then 
        echo "$TOTAL_TODAY_CONNECTION_FAILURE_BY_USER" >> "$OUTPUT_ERROR_FILE"
    else
        echo "No failed connection found today." >> "$OUTPUT_ERROR_FILE"
    fi 
    
    echo "" >> "$OUTPUT_ERROR_FILE"
    echo "******************************************************************************************************" >> "$OUTPUT_ERROR_FILE"

}

#script
#ensure all needed directories exists
check_and_create_directory $OUTPUT_DIRECTORY
check_and_create_directory $OUTPUT_LOGS_DIRECTORY

#ensure all needed logfiles exist
for logfile in "${LOG_FILES_LIST[@]}"; do
    isfileexists "$logfile"
done

#check if output file exists
if [ -e "$OUTPUT_ERROR_FILE" ]; then
        echo -e "${GREEN}OK : $OUTPUT_ERROR_FILE exists.${NC}"
else
    echo -e "${RED}KO : $OUTPUT_ERROR_FILE doesn't exist.${NC}"
    touch $OUTPUT_ERROR_FILE
    chmod 664 $OUTPUT_ERROR_FILE
    isfileexists $OUTPUT_ERROR_FILE
fi

#top 10 failure connection of the day 
echo "" >> "$OUTPUT_ERROR_FILE"
echo "1-FAILED AUTHENTIFICATION" >> "$OUTPUT_ERROR_FILE"
failed_connection_errors

#top 10 errors of each logfile
echo "2-ERRORS IN LOGS" >> "$OUTPUT_ERROR_FILE"
echo "" >> "$OUTPUT_ERROR_FILE"
count_and_display_errors  "$COMMAND1" "${GROUP1[@]}"
count_and_display_errors  "$COMMAND2" "${GROUP2[@]}"
count_and_display_errors  "$COMMAND3" "${GROUP3[@]}"
count_and_display_errors  "$COMMAND4" "${GROUP4[@]}"
count_and_display_errors  "$COMMAND5" "${GROUP5[@]}"
count_and_display_errors  "$COMMAND6" "${GROUP6[@]}"
count_and_display_errors  "$COMMAND7" "${GROUP7[@]}"

#delete output files older than 1 month 
delete_old_log_files
