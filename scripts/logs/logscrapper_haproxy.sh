#!/bin/bash

#variables
# Define color codes
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
BLUE='\033[0;34m'
NC='\033[0m'


CURRENT_DATE=$(date "+%d%m%Y_%H%M")
OUTPUT_DIRECTORY="/data/logscrapper"
OUTPUT_LOGS_DIRECTORY="$OUTPUT_DIRECTORY/logs"
HAPROXY_LOGS="/opt/collabnet/teamforge/log/haproxy/haproxy.log"
OUTPUT_ERROR_FILE="$OUTPUT_LOGS_DIRECTORY/errorfile${CURRENT_DATE}.txt"

COMMAND1="sudo grep -E '[45][0-9][0-9]' \$HAPROXY_LOGS | awk '{ if (\$11 ~ /^4|^5/) print \$1, \$2, \$6, \$8, \$11 }' | sort | uniq -c | head -n 10 | sort -nr"

#functions 
isfileexists() {
    if [ -e "$1" ]; then
        echo -e "${GREEN}OK : $1 exists.${NC}"
    else
        echo -e "${RED}KO : $1 does not exist.${NC}"
        exit 1
    fi
}

check_and_create_directory() {
    local DIRECTORY="$1"

    if [ ! -d "$DIRECTORY" ]; then
        mkdir -p "$DIRECTORY"
        echo -e "${GREEN}Directory '$DIRECTORY' created.${NC}"
    else
        echo -e "${BLUE}Directory '$DIRECTORY' already exists.${NC}"
    fi
}

delete_old_log_files() {
    local OLD_OUTPUT_FILES=$(find "$OUTPUT_LOGS_DIRECTORY" -maxdepth 1 -type f -name "errorfile*.txt" -mtime +31)
    if [ -n "$OLD_OUTPUT_FILES" ]; then
        echo -e "${YELLOW}Deleting log files older than 31 days...${NC}"
        echo "$OLD_OUTPUT_FILES" | xargs rm -f
        echo -e "${GREEN}OK : Old log files deleted.${NC}"
    else
        echo -e "${BLUE}INFO : No log files older than 31 days found.${NC}"
    fi
}

haproxy_connections() {
    local haproxy_logs="$1"
    local connection_logs="$2"


    echo "2-HAPROXY CONNECTION" >> "$OUTPUT_ERROR_FILE"
    echo >> "$OUTPUT_ERROR_FILE"

    if [ -n "$connection_logs" ]; then
        eval "$connection_logs" >> "$OUTPUT_ERROR_FILE"
    else 
        echo "No haproxy connection found today." >> "$OUTPUT_ERROR_FILE"
    fi 
}

failed_connection_errors(){
    local TOTAL_TODAY_UNIQ_USER_CONNECTION_FAILURE=$(sudo journalctl -u sshd --since "-1d" | grep "failed" | awk '$0 != "" { if (!seen[$0]++) print $1, $2, $4, $7, $14, $15 }' | uniq -c | sort -nr | head -n 10 | wc -l)
    local TOTAL_TODAY_CONNECTION_FAILURE_BY_USER=$(sudo journalctl -u sshd --since "-1d" | grep "failed" | awk '$0 != "" { if (!seen[$0]++) print $1, $2, $4, $7, $14, $15 }' | uniq -c | sort | head -n 10 | sort -nr)
    echo "1-FAILED AUTHENTICATION" >> "$OUTPUT_ERROR_FILE"
    echo >> "$OUTPUT_ERROR_FILE"

    if [ "$TOTAL_TODAY_UNIQ_USER_CONNECTION_FAILURE" -gt 0 ]; then 
        echo "$TOTAL_TODAY_CONNECTION_FAILURE_BY_USER" >> "$OUTPUT_ERROR_FILE"
    else
        echo "No failed connection found today." >> "$OUTPUT_ERROR_FILE"
    fi 
    
    echo >> "$OUTPUT_ERROR_FILE"
    echo "******************************************************************************************************" >> "$OUTPUT_ERROR_FILE"
}

#script
#ensure all needed directories exists
check_and_create_directory $OUTPUT_DIRECTORY
check_and_create_directory $OUTPUT_LOGS_DIRECTORY

#check haproxy file exists
isfileexists "$HAPROXY_LOGS"

#top 10 failure connection of the day 
failed_connection_errors

#check and display existing haproxy connections
haproxy_connections "$HAPROXY_LOGS" "$COMMAND1"

#delete output files older than 1 month 
delete_old_log_files

