#!/bin/bash

#variables
# Define color codes
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
BLUE='\033[0;34m'
NC='\033[0m'

CURRENT_DATE=$(date "+%d%m%Y_%H%M")
OUTPUT_DIRECTORY="/data/logscrapper"
OUTPUT_LOGS_DIRECTORY="$OUTPUT_DIRECTORY/logs"
OUTPUT_ERROR_FILE="$OUTPUT_LOGS_DIRECTORY/errorfile${CURRENT_DATE}.txt"
LOG_FILES=("/opt/collabnet/teamforge/log/pgsql/postgresql.log")
POSTGRESQL_LOGS="/opt/collabnet/teamforge/log/pgsql/postgresql.log"

#functions 
isfileexists() {
    if [ -e "$1" ]; then
        echo -e "${GREEN}OK : $1 exists.${NC}"
    else
        echo -e "${RED}KO : $1 does not exist.${NC}"
        exit 1
    fi
}

check_and_create_directory() {
    local DIRECTORY="$1"

    if [ ! -d "$DIRECTORY" ]; then
        mkdir -p "$DIRECTORY"
        echo -e "${GREEN}Directory '$DIRECTORY' created.${NC}"
    else
        echo -e "${RED}Directory '$DIRECTORY' already exists.${NC}"
    fi
}

delete_old_log_files() {
    local OLD_OUTPUT_FILES=$(find "$OUTPUT_LOGS_DIRECTORY" -maxdepth 1 -type f -name "errorfile*.txt" -mtime +31)
    if [ -n "$OLD_OUTPUT_FILES" ]; then
        echo -e "${YELLOW}Deleting log files older than 31 days...${NC}"
        echo "$OLD_OUTPUT_FILES" | xargs rm -f
        echo -e "${GREEN}OK : Old log files deleted.${NC}"
    else
        echo -e "${BLUE}INFO : No log files older than 31 days found.${NC}"
    fi
}

pgsql_logs() {
    local pgsql_logs="$1"
    local database_logs=$(sudo grep "LOG:" "/opt/collabnet/teamforge/log/pgsql/postgresql.log" | awk '$0 != "" { if (!seen[$0]++){print $1, $3, $4, $5, $6}}' | sort | uniq -c | head -n 10 | sort -nr)
    
    echo "2-PGSQL CONNECTION" >> "$OUTPUT_ERROR_FILE"
    echo >> "$OUTPUT_ERROR_FILE"

    if [ -n "$database_logs" ]; then
        echo "$database_logs" >> "$OUTPUT_ERROR_FILE"
    else 
        echo "No pgsql connection found today." >> "$OUTPUT_ERROR_FILE"
    fi 
}

failed_connection_errors(){
    local TOTAL_TODAY_UNIQ_USER_CONNECTION_FAILURE=$(sudo journalctl -u sshd --since "-1d" | grep "failed" | awk '$0 != "" { if (!seen[$0]++) print $1, $2, $4, $7, $14, $15 }' | uniq -c | sort -nr | head -n 10 | wc -l)
    local TOTAL_TODAY_CONNECTION_FAILURE_BY_USER=$(sudo journalctl -u sshd --since "-1d" | grep "failed" | awk '$0 != "" { if (!seen[$0]++) print $1, $2, $4, $7, $14, $15 }' | uniq -c | sort -nr | head -n 10)
    echo "1-FAILED AUTHENTICATION" >> "$OUTPUT_ERROR_FILE"
    echo >> "$OUTPUT_ERROR_FILE"

    if [ "$TOTAL_TODAY_UNIQ_USER_CONNECTION_FAILURE" -gt 0 ]; then 
        echo "$TOTAL_TODAY_CONNECTION_FAILURE_BY_USER" >> "$OUTPUT_ERROR_FILE"
    else
        echo "No failed connection found today." >> "$OUTPUT_ERROR_FILE"
    fi 
    
    echo >> "$OUTPUT_ERROR_FILE"
    echo "******************************************************************************************************" >> "$OUTPUT_ERROR_FILE"
}

#script
#ensure all needed directories exists
check_and_create_directory $OUTPUT_DIRECTORY
check_and_create_directory $OUTPUT_LOGS_DIRECTORY

#check pgsql log files exist
isfileexists "$POSTGRESQL_LOGS"

#check if output file exists
if [ -e "$OUTPUT_ERROR_FILE" ]; then
        echo -e "${GREEN}OK : $OUTPUT_ERROR_FILE exists.${NC}"
else
    echo -e "${RED}KO : $OUTPUT_ERROR_FILE doesn't exist.${NC}"
    sudo touch $OUTPUT_ERROR_FILE
    sudo chmod 664 $OUTPUT_ERROR_FILE
    echo -e "${GREEN}OK : $OUTPUT_ERROR_FILE created.${NC}"
fi

#top 10 failure connection of the day 
failed_connection_errors

#check and display existing pgsql connections
pgsql_logs "$POSTGRESQL_LOGS"

#delete output files older than 1 month 
delete_old_log_files

