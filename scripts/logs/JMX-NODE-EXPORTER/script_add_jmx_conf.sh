#!/bin/sh
########################################################################################
#   Signal chaining (http://java.sun.com/j2se/1.4.2/docs/guide/vm/signal-chaining.html)
#   This is required because some jni code that accesses sockets is conflicting with the
#   jvm signalling mechanism. This is a workaround.

CATALINA_OPTS="$CATALINA_OPTS -Djava.net.preferIPv4Stack=true -Djava.rmi.server.hostname=hostnameWithJavaagent -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -Djava.util.logging.config.file=logging.properties -javaagent:/data/jvm_monitoring/jmx/jmx_prometheus_javaagent-0.18.0.jar=9130:/data/jvm_monitoring/jmx/config.yaml -Xloggc:/data/jvm_monitoring/gc.log -XX:+PrintGCDetails -XX:+PrintGCDateStamps  -XX:+UseGCLogFileRotation -XX:GCLogFileSize=5M"

# Due to threading issues on some Linux distributions, sometimes an
# environment variable must be set.
if [ -r "$CATALINA_HOME"/bin/assume_kernel.sh ]; then
  . "$CATALINA_HOME"/bin/assume_kernel.sh
fi

if [ -z "$JRE_HOME" ] ; then
        KW_JAVA_HOME=$JAVA_HOME
else
        KW_JAVA_HOME=$JRE_HOME
fi
K_UNAME=`uname -s 2> /dev/null`
K_PRELOAD=libjsig.so
case "$K_UNAME" in
        *[sS][oO][lL][aA][rR][iI][sS]*|*[sS][uU][nN][oO][sS]*) LD_LIBRARY_PATH="$KW_JAVA_HOME"/lib/sparc/;;
        *[lL][iI][nN][uU][xX]*)
                if [ -d "$KW_JAVA_HOME/lib/i386/" ]
                then
                        LD_LIBRARY_PATH="$KW_JAVA_HOME"/lib/i386/
                elif [ -d "$KW_JAVA_HOME/lib/amd64/" ]
                then
                        LD_LIBRARY_PATH="$KW_JAVA_HOME"/lib/amd64/
                else
                        K_PRELOAD=
                fi
        ;;
esac
LD_PRELOAD=$K_PRELOAD
export LD_PRELOAD
export LD_LIBRARY_PATH

#JAVA_OPTS=-verbose:class
JAVA_OPTS="-XX:MaxPermSize=128m" $JAVA_OPTS
export JAVA_OPTS

echo "REP_HOME=$REP_HOME"
echo "PROJECTS_ROOT=$PROJECTS_ROOT"
~
~
~
~
~
~
~
