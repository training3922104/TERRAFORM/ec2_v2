#!/bin/bash

#variables
# Define color codes
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
BLUE='\033[0;34m'
NC='\033[0m'

CURRENT_DATE=$(date "+%d%m%Y_%H%M")

#all log files to survey
STRING_TO_GREP="error"

OUTPUT_DIRECTORY="/data/logscrapper"
OUTPUT_LOGS_DIRECTORY="$OUTPUT_DIRECTORY/logs"
OUTPUT_ERROR_FILE="$OUTPUT_LOGS_DIRECTORY/errorfile${CURRENT_DATE}.txt"
NEXUS_DIRECTORY="/data/sonatype-work/nexus3/log"
NEXUS_LOG_FILES="$NEXUS_DIRECTORY/nexus.log"
NEXUS_REQUEST_LOG_FILES="$NEXUS_DIRECTORY/request.log"

LOG_FILES_LIST=(
    "$NEXUS_LOG_FILES"
    "$NEXUS_REQUEST_LOG_FILES"
)

GROUP1=("$NEXUS_LOG_FILES")
GROUP2=("$NEXUS_REQUEST_LOG_FILES")

COMMAND1="sudo grep -ri \$STRING_TO_GREP \$file_path | awk '\$0 != \"\" { if (!seen[\$0]++){print \$3, \$4, \$5, \$6 }}' | sort | uniq -c | head -n 10 | sort -nr"
COMMAND2="sudo grep -E '[45][0-9][0-9]' \$file_path | awk '{ if (\$9 ~ /^4|^5/) print \$1, \$2, \$6, \$8, \$9 }' | sort | uniq -c | head -n 10 | sort -nr"

#functions 
isfileexists() {
    if [ -e "$1" ]; then
        echo -e "${GREEN}OK : $1 exists.${NC}"
    else
        echo -e "${YELLOW}INFO : $1 don't exist yet.${NC}"
        exit 1
    fi
}

isfileaccessible() {
    local file="$1"
    if [ -r "$file" ]; then
        echo -e "${GREEN}OK : $file is accessible.${NC}"
    else
        echo -e "${RED}Error : $file is not accessible or permission denied.${NC}"
        exit 1
    fi
}

check_and_create_directory() {
    local DIRECTORY="$1"

    if [ ! -d "$DIRECTORY" ]; then
        mkdir -p "$DIRECTORY"
        echo -e "${GREEN}Directory '$DIRECTORY' created.${NC}"
    else
        echo -e "${RED}Directory '$DIRECTORY' already exists.${NC}"
    fi
}

delete_old_log_files() {
    local OLD_OUTPUT_FILES=$(find "$OUTPUT_LOGS_DIRECTORY" -maxdepth 1 -type f -name "errorfile*.txt" -mtime +31)
    if [ -n "$OLD_OUTPUT_FILES" ]; then
        echo -e "${YELLOW}Deleting log files older than 31 days...${NC}"
        echo "$OLD_OUTPUT_FILES" | xargs rm -f
        echo -e "${GREEN}OK : Old log files deleted.${NC}"
    else
        echo -e "${BLUE}INFO : No log files older than 31 days found.${NC}"
    fi
}

count_and_display_errors() {
    local command_to_run="$1"
    shift
    local group_name="$@"
    error_count=0
    
    for file_path in ${group_name[@]}; do
        error_count=$(eval "$command_to_run" | wc -l)
        if isfileaccessible "$file_path"; then
            echo -e "${YELLOW}INFO : processing $file_path.${NC}"  
            echo "******************************************************************************************************" >> "$OUTPUT_ERROR_FILE"
            if [ "$error_count" -eq 0 ] || [ "$error_count" -eq 1 ]; then
                echo -e "$file_path" : $error_count ERROR >> "$OUTPUT_ERROR_FILE"
            else
                echo -e "$file_path" : $error_count ERRORS >> "$OUTPUT_ERROR_FILE"
            fi
            echo "******************************************************************************************************" >> "$OUTPUT_ERROR_FILE"

            if [ $error_count -eq 0 ]; then
                    echo -e "${GREEN}OK : No error found in $file_path${NC}"
                    echo >> "$OUTPUT_ERROR_FILE"
                    echo "No error found in $file_path" >> "$OUTPUT_ERROR_FILE"
                    echo >> "$OUTPUT_ERROR_FILE"
            else
                    echo >> "$OUTPUT_ERROR_FILE"
                    eval "$command_to_run" >> "$OUTPUT_ERROR_FILE"
                    echo >> "$OUTPUT_ERROR_FILE"
            fi
        fi
    done
}

failed_connection_errors(){
    local TOTAL_TODAY_UNIQ_USER_CONNECTION_FAILURE=$(sudo journalctl -u sshd --since "-1d" | grep "failed" | awk '$0 != "" { if (!seen[$0]++) print $1, $2, $4, $7, $14, $15 }' | uniq -c | sort -nr | head -n 10 | wc -l)
    local TOTAL_TODAY_CONNECTION_FAILURE_BY_USER=$(sudo journalctl -u sshd --since "-1d" | grep "failed" | awk '$0 != "" { if (!seen[$0]++) print $1, $2, $4, $7, $14, $15 }' | uniq -c | sort -nr | head -n 10)
    echo "" >> "$OUTPUT_ERROR_FILE"

    if [ $TOTAL_TODAY_UNIQ_USER_CONNECTION_FAILURE -gt 0 ]; then 
        echo "$TOTAL_TODAY_CONNECTION_FAILURE_BY_USER" >> "$OUTPUT_ERROR_FILE"
    else
        echo "No failed connection found today." >> "$OUTPUT_ERROR_FILE"
    fi 
    
    echo "" >> "$OUTPUT_ERROR_FILE"
    echo "******************************************************************************************************" >> "$OUTPUT_ERROR_FILE"
}

#script
#ensure all needed directories exists
check_and_create_directory $OUTPUT_DIRECTORY
check_and_create_directory $OUTPUT_LOGS_DIRECTORY

#ensure all needed logfiles exist
for logfile in "${LOG_FILES_LIST[@]}"; do
    isfileexists "$logfile"
done

#check if output file exists
if [ -e "$OUTPUT_ERROR_FILE" ]; then
        echo -e "${GREEN}OK : $OUTPUT_ERROR_FILE exists.${NC}"
else
    echo -e "${RED}KO : $OUTPUT_ERROR_FILE doesn't exist.${NC}"
    touch $OUTPUT_ERROR_FILE
    chmod 664 $OUTPUT_ERROR_FILE
    isfileexists $OUTPUT_ERROR_FILE
fi

#top 10 failure connection of the day 
echo "" >> "$OUTPUT_ERROR_FILE"
echo "1-FAILED AUTHENTIFICATION" >> "$OUTPUT_ERROR_FILE"
failed_connection_errors

#top 10 errors of each logfile
echo "2-ERRORS IN LOGS" >> "$OUTPUT_ERROR_FILE"
echo "" >> "$OUTPUT_ERROR_FILE"
count_and_display_errors  "$COMMAND1" "${GROUP1[@]}"
count_and_display_errors  "$COMMAND2" "${GROUP2[@]}"

#delete output files older than 1 month 
delete_old_log_files


