#!/bin/bash
#documentation
#https://github.com/cybertec-postgresql/pgwatch2

#touch install_pgwatch2.sh ; sudo chmod +x install_pgwatch2.sh ; sudo vim install_pgwatch2.sh

#variables
# Define color codes
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
BLUE='\033[0;34m'
NC='\033[0m'

PGWATCH2_VERSION_LATEST=$(curl -so- https://api.github.com/repos/cybertec-postgresql/pgwatch2/releases/latest | jq .tag_name | grep -oE '[0-9\.]+')

#functions 
check_docker_status() {
    if systemctl is-active --quiet docker; then
        echo -e "${GREEN}OK : Docker is running.${NC}"
    else
        echo -e "${RED}KO : Docker is not running.${NC}"
        exit 1
    fi
}

docker_gpg_setup() {
    if [ ! -f /etc/apt/trusted.gpg.d/docker.gpg ]; then
        echo -e "${YELLOW}INFO : Downloading Docker GPG key...${NC}"
        if curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/docker.gpg; then
            echo "${GREEN}OK : Docker GPG key downloaded successfully.${NC}"
        else
            echo -e "${RED}KO : Failed to download Docker GPG key.${NC}"
            exit 1
        fi
    else
        echo -e "${BLUE}INFO : Docker GPG key already exists.${NC}"
    fi
}

# Install Docker
sudo apt update
sudo apt install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common \
    jq


docker_gpg_setup

sudo apt install -y docker.io
sudo usermod -aG docker $USER

#check if docker is running
check_docker_status

# Pull the latest image
if sudo docker pull cybertec/pgwatch2-postgres; then
    # Run the container
    container_id=$(sudo docker run -d --restart=unless-stopped -p 3000:3000 -p 8080:8080 -p 8081:8081 -p 127.0.0.1:5432:5432 --name pw2 cybertec/pgwatch2-postgres:$PGWATCH2_VERSION_LATEST)

    # Output the container ID
    echo -e "${GREEN}OK : succeeded to pull the Docker image.${NC}"
    echo -e "${BLUE}INFO : Container_ID $container_id .${NC}"
else
    echo -e "${RED}KO : Failed to pull the Docker image.${NC}"
    exit 1
fi

# Install Docker Compose
# DOCKER_CONFIG=${DOCKER_CONFIG:-$HOME/.docker}
# mkdir -p $DOCKER_CONFIG/cli-plugins
# curl -SL https://github.com/docker/compose/releases/download/v2.24.6/docker-compose-linux-x86_64 -o $DOCKER_CONFIG/cli-plugins/docker-compose
# chmod +x $DOCKER_CONFIG/cli-plugins/docker-compose

# Create directory for pgwatch2 and navigate into it
# mkdir pgwatch2
# cd pgwatch2

# Download Docker Compose configuration
# curl -o docker-compose.yml https://raw.githubusercontent.com/cybertec-postgresql/pgwatch2/master/docker-compose.yml

# Start pgwatch2
# sudo docker-compose up -d

#check http://34.239.182.244:3000


# sudo docker logs pw2
# sudo docker kill pw2
# sudo docker rm pw2


#influxDB
#documentation : https://docs.influxdata.com/influxdb/v2/install/
