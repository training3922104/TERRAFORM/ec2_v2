#!/bin/bash

# touch install_ansible.sh ; sudo chmod +x install_ansible.sh ; sudo vim install_ansible.sh

#variables
# Define color codes
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

#others 
ansible_cfg="/path/to/ansible.cfg"

sudo apt update
# sudo apt install -y python3-pip
# pip3 install ansible==9.1.0
# pip3 install boto3
sudo apt install -y awscli
sudo apt install -y ansible
sudo mkdir ~/setup_environment
sudo mkdir ~/setup_environment/bin ~/setup_environment/playbooks ~/setup_environment/inventory ~/setup_environment/plugin ~/setup_environment/roles
cd ~/setup_environment
touch ~/setup_environment/ansible.cfg
#sed -i 's/^#enable_plugins = aws_ec2/enable_plugins = aws_ec2/' "$ansible_cfg"

if [[ ":$PATH:" != *":/home/ubuntu/.local/bin:"* ]]; then
    export PATH="$PATH:/home/ubuntu/.local/bin"
fi
ansible-galaxy collection install amazon.aws

ansible --version

##EC2 Inventory
# DOC https://docs.ansible.com/ansible/latest/collections/amazon/aws/aws_ec2_inventory.html

curl -o ~/aws_ec2.py https://raw.githubusercontent.com/ansible/ansible/stable-2.9/contrib/inventory/ec2.py
curl -o ~/aws_ec2.ini https://raw.githubusercontent.com/ansible/ansible/stable-2.9/contrib/inventory/ec2.ini
# chmod +x /path/to/ansible/aws_ec2.py
# /path/to/ansible/aws_ec2.py --list test 
# ansible-playbook -i /path/to/ansible/aws_ec2.py your_playbook.yml use inventory
ansible-inventory -i /usr/lib/python3/dist-packages/ansible_collections/amazon/aws/tests/integration/targets/inventory_aws_ec2/test.aws_ec2.yml --graph
echo -e "${GREEN}Ansible installation and configuration completed.${NC}"


#launch ansible-galaxy command from /home/ubuntu/.local/bin