#!/bin/bash

#touch install_haproxy.sh ; sudo chmod +x install_haproxy.sh ; sudo vim install_haproxy.sh

#variables
# Define color codes
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
NC='\033[0m'

FRONTEND_SERVER_IP="54.196.222.177"
BACKEND_SERVERS_IP=("54.196.145.143" "35.175.196.5")

HAPROXY_CFG_PATH="/etc/haproxy/haproxy.cfg"

#functions
isservicesactive () {
    if systemctl is-active --quiet $1; then
        echo -e "${GREEN}$1 is running.${NC}"
        return 0
    else
        echo -e "${RED}$1 is not running.${NC}"
        return 1
    fi
}

check_ping() {
    local target="$1"
    if ping -c 1 "$target" &> /dev/null; then
        echo -e "${GREEN}OK : Ping to $target was successful.${NC}"
    else
        echo -e "${RED}KO : Ping to $target failed.${NC}"
        exit 1
    fi
}

edit_haproxy_cfg() {
    local servers=("$@")
    local server_config=""

    # Generate server configurations
    for ((i=0; i<${#servers[@]}; i++)); do
        server_config+="        server server$i ${servers[i]}:443 check"$'\n'
    done

    # Append configuration to haproxy.cfg
    sudo tee -a "$HAPROXY_CFG_PATH" <<EOF

listen stats
        bind *:8080
        mode http
        stats enable
        stats uri /haproxy_stats
        stats realm HAProxy\ Statistics
        stats auth admin:admin

frontend http_front
        bind *:80
        mode tcp
        default_backend http_back
        acl frontend_server_acl src $FRONTEND_SERVER_IP/32
        tcp-request content accept if frontend_server_acl

backend http_back
        mode http
        balance roundrobin
$server_config
EOF

    echo -e "${GREEN}Servers added to haproxy.cfg${NC}"
}


#script
sudo apt update
sudo apt install -y haproxy

#check ping of servers
check_ping "$FRONTEND_SERVER_IP"
for server_ip in "${BACKEND_SERVERS_IP[@]}"; do
    check_ping "$server_ip"
done

#edit haproxy.cfg
edit_haproxy_cfg "${BACKEND_SERVERS_IP[@]}"

sudo systemctl restart haproxy.service

if [ $? -eq 0 ]; then
    echo -e "${GREEN}OK : HAProxy service restarted successfully.${NC}"
else
    echo -e "${RED}ERROR : Failed to restart HAProxy service.${NC}"
    exit 1
fi

if isservicesactive haproxy.service; then
    echo -e "${GREEN}OK : Haproxy is active.${NC}"
else
    echo -e "${RED}KO : Haproxy is not active.${NC}"
    exit 1
fi

echo -e "${GREEN}Haproxy installation and configuration completed.${NC}"