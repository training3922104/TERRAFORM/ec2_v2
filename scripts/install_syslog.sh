#!/bin/bash

#touch install_grafana.sh ; sudo chmod +x install_grafana.sh ; sudo vim install_grafana.sh

#variables
# Define color codes
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

sudo apt update
sudo apt install rsyslog
sudo nano /etc/rsyslog.conf

echo -e "${GREEN}Grafana installation and configuration completed.${NC}"
