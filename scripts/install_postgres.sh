#!/bin/bash

#touch install_postgres.sh ; sudo chmod +x install_postgres.sh ; sudo vim install_postgres.sh

#variables
# Define color codes
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
NC='\033[0m'

DB_NAME="training_database"
DB_USER="postgres"
DB_PASSWORD="postgres"
PGWATCH_SERVER_IP="3.95.153.195"
LINE="$DB_USER ALL=(ALL) ALL"
SUDOERS_FILE="/etc/sudoers.d/$DB_USER"
POSTGRES_CONF_FILE="/etc/postgresql/14/main/postgresql.conf"
POSTGRES_HBA_CONF_FILE="/etc/postgresql/14/main/pg_hba.conf"
HOST_ENTRY="host    $DB_NAME    $DB_USER    $PGWATCH_SERVER_IP/32    md5"
TEMP_FILE=$(mktemp)
NEW_SERVER_NAME="postgres-server"

#functions

change_prompt() {
    # Backup the original .bashrc
    cp ~/.bashrc ~/.bashrc_backup

    # Replace hostname in .bashrc
    sed -i "s/\\h/$1/g" ~/.bashrc

    # Source .bashrc to apply changes
    source ~/.bashrc

    # Check if modification was successful
    if grep -q "ubuntu@$1" ~/.bashrc; then
        echo -e "${GREEN}OK : Prompt changed to ubuntu@$1.${NC}"
    else
        echo -e "${RED}Error: Prompt modification failed.${NC}"
    fi
}

ispostgresactive () {
    if systemctl is-active --quiet postgresql; then
        echo -e "${GREEN}PostgreSQL is running.${NC}"
        return 0
    else
        echo -e "${RED}PostgreSQL is not running.${NC}"
        return 1
    fi
}

check_ping() {
    if ping -c 1 "$1" &> /dev/null; then
        echo -e "${GREEN}OK : Ping to $1 was successful.${NC}"
    else
        echo -e "${RED}KO : Ping to $1 failed.${NC}"
        exit 1
    fi
}

change_postgres_listen_address() {

    # Check if the postgres.conf file exists
    if [ ! -f "$POSTGRES_CONF_FILE" ]; then
        echo "Error: postgresql.conf file not found at $POSTGRES_CONF_FILE"
        return 1
    fi

    # Modify the listen_addresses setting in postgresql.conf using sed
    sed 's/#listen_addresses = '\''localhost'\''/listen_addresses = '\''*'\''/' "$POSTGRES_CONF_FILE" > "$TEMP_FILE" && mv -f "$TEMP_FILE" "$POSTGRES_CONF_FILE"

    # Check if sed operation was successful
    if [ $? -eq 0 ]; then
    echo -e "${GREEN}Replacement successful.${NC}"
    sudo systemctl restart postgres
        if ispostgresactive; then
            echo -e "${GREEN}OK : PostgreSQL has been restarted.${NC}"
        else 
            echo -e "${RED}KO : PostgreSQL has not been restarted.${NC}"
            exit 1
        fi
    else
        echo -e "${RED}KO : Replacement failed.${NC}"
        exit 1
    fi
    # Change ownership of the postgresql.conf file to postgres user
    sudo chown postgres:postgres "$POSTGRES_CONF_FILE"

    # Check if chown operation was successful
    if [ $? -ne 0 ]; then
        echo -e "${RED}KO : Failed to change ownership of postgresql.conf.${NC}"
        return 1
    fi

    echo -e "${GREEN}OK : PostgreSQL listen_addresses setting modified successfully.${NC}"
}

#rename prompt
change_prompt

#script
sudo apt update
sudo apt install -y postgresql postgresql-contrib

check_ping $PGWATCH_SERVER_IP

#edit prometheus.conf to allow any servers to access ddb
change_postgres_listen_address


#TODO find a way to not use chown
#TODO create a function 


#edit pg_hba.conf

if [ ! -f "$POSTGRES_HBA_CONF_FILE" ]; then
    echo -e "${RED}pg_hba.conf not found in $POSTGRES_HBA_CONF_FILE.${NC}"
    exit 1
fi

if grep -qF "$HOST_ENTRY" "$POSTGRES_HBA_CONF_FILE"; then
    echo -e "${GREEN}OK : Entry already exists in pg_hba.conf.${NC}"
else
    echo -e "$HOST_ENTRY" | sudo tee -a "$POSTGRES_HBA_CONF_FILE" >/dev/null
    echo -e "${GREEN}OK : Entry added to pg_hba.conf ${NC}"
fi


echo -e "${GREEN}OK : PostgreSQL installation and configuration completed.${NC}"

if [ ! -f "$SUDOERS_FILE" ]; then
    echo -e "${YELLOW}Creating sudoers file: $SUDOERS_FILE.${NC}"
    sudo touch "$SUDOERS_FILE"
fi

if grep -qF "$LINE" "$SUDOERS_FILE"; then
    echo -e "${GREEN}OK : The line '$LINE' already exists in $SUDOERS_FILE.${NC}"
else
    echo -e "${YELLOW}Adding the line '$LINE' to $SUDOERS_FILE.${NC}"
    echo "$LINE" | sudo tee -a "$SUDOERS_FILE" > /dev/null
    echo -e "${GREEN}OK : Postgres user is added to sudoers.${NC}"
fi
#access db 
PSQL_COMMAND="sudo -i -u postgres psql"
CREATE_DB="CREATE DATABASE $DB_NAME;"
CONNECT_DB="\c $DB_NAME;"
CREATE_TABLE="CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    username VARCHAR(50),
    email VARCHAR(100),
    age INT
);"
INSERT_DATA="INSERT INTO users (username, email, age) VALUES
('john_doe', 'john@example.com', 30),
('jane_smith', 'jane@example.com', 25),
('bob_johnson', 'bob@example.com', 40);"
SELECT_DATA="SELECT * FROM users;"
CREATE_ROLE_PGWATCH="CREATE ROLE pgwatch2 WITH LOGIN PASSWORD 'secret';"
CREATE_MONITOR_ROLE="CREATE ROLE pgmonitor;"
GRANT_ROLE="GRANT pgmonitor TO pgwatch2;"
ALTER_ROLE="ALTER ROLE pgwatch2 CONNECTION LIMIT 3;"
CREATE_EXTENSION="CREATE EXTENSION pg_stat_statements;"

# Execute PostgreSQL commands
$PSQL_COMMAND -c "$CREATE_DB"
$PSQL_COMMAND -c "$CONNECT_DB"
$PSQL_COMMAND -c "$CREATE_TABLE"
$PSQL_COMMAND -c "$INSERT_DATA"
$PSQL_COMMAND -c "$SELECT_DATA"
$PSQL_COMMAND -c "$CREATE_ROLE_PGWATCH"
$PSQL_COMMAND -c "$CREATE_MONITOR_ROLE"
$PSQL_COMMAND -c "$GRANT_ROLE"
$PSQL_COMMAND -c "$ALTER_ROLE"
$PSQL_COMMAND -c "$CREATE_EXTENSION"

# sudo -i -u postgres
# psql
# CREATE DATABASE ftraining_database;
# \c training_database;
# CREATE TABLE users (
#     id SERIAL PRIMARY KEY,
#     username VARCHAR(50),
#     email VARCHAR(100),
#     age INT
# );

# INSERT INTO users (username, email, age) VALUES
# ('john_doe', 'john@example.com', 30),
# ('jane_smith', 'jane@example.com', 25),
# ('bob_johnson', 'bob@example.com', 40);

# SELECT * FROM users;

#sudo -i -u postgres psql -c "CREATE DATABASE training_database;"
# if psql -lqt | cut -d \| -f 1 | grep -qw "$DB_NAME"; then
#     echo "Database '$DB_NAME' already exists. Exiting."
#     exit 1
# fi

# psql -U "$DB_USER" -c "CREATE DATABASE $DB_NAME;"

# if [ $? -eq 0 ]; then
#     echo -e "${GREEN}Database '$DB_NAME' created successfully.${NC}"
# else
#     echo -e "${RED}Failed to create database '$DB_NAME'.${NC}"
#     exit 1
# fi

#psql
#\l : check database list 
#\c databaseName : connect to a database
#\q : quit psql