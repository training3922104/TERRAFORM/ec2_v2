#!/bin/bash

#touch install_postgres.sh ; sudo chmod +x install_postgres.sh ; sudo vim install_postgres.sh

#variables
# Define color codes
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
NC='\033[0m'

DB_NAME="training_database"
DB_USER="postgres"
DB_PASSWORD="postgres"
DB_HOSTNAME="3.95.152.213"
LINE="$DB_USER ALL=(ALL) ALL"
SUDOERS_FILE="/etc/sudoers.d/$DB_USER"

#functions



#script 
sudo su << SCRIPT
wget https://github.com/prometheus-community/postgres_exporter/releases/download/v0.9.0/postgres_exporter-0.9.0.linux-amd64.tar.gz
tar xvfz postgres_exporter-*.linux-amd64.tar.gz
cd postgres_exporter-*.linux-amd64

export DATA_SOURCE_NAME='postgresql://postgres:$DB_PASSWORD@$DB_HOSTNAME:5432/postgres?sslmode=disable'

check_ping "$PROMETHEUS_SERVER_IP"
isfileexists "$GRAFANA_ALLOY_CONFIG_FILE"

# Add the new configuration directly to the original file
tee "$GRAFANA_ALLOY_CONFIG_FILE" > /dev/null << EOF
// Sample config for Alloy.
//
// For a full configuration reference, see https://grafana.com/docs/alloy
logging {
  level = "warn"
}

prometheus.exporter.unix "default" {
  include_exporter_metrics = true
  disable_collectors       = ["mdadm"]
}

prometheus.scrape "default" {
  targets    = concat(
       prometheus.exporter.unix.default.targets,
       [{
         // Self-collect metrics
         job         = "alloy",
         __address__ = "54.235.42.27:12345",
       }],
     )
  forward_to = [prometheus.remote_write.default.receiver]
 }

prometheus.remote_write "default" {
       endpoint {
               url = "http://$PROMETHEUS_SERVER_IP:9090/write"
       }
}
EOF

if [ $? -eq 0 ]; then
    echo -e "${GREEN}OK : Configuration file updated.${NC}"
else
    echo -e "${RED}KO : Failed to update configuration file.${NC}"
    exit 1
fi

systemctl enable alloy.service
systemctl start alloy.service
isservicesactive alloy.service
SCRIPT
