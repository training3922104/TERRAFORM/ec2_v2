#!/bin/bash

# touch ssh_copy_id.sh ; sudo chmod +x ssh_copy_id.sh ; sudo vim ssh_copy_id.sh

##Variables
# Define color codes
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
NC='\033[0m'

USERNAME="ubuntu"
SSH_DIRECTORY="~/.ssh/"
PRIVATE_KEY="$HOME/.ssh/id_rsa"
PUBLIC_KEY="$HOME/.ssh/id_rsa.pub"
IP_ADDRESSES=("54.87.52.72")
REMOTE_USER="ubuntu"

# Function to generate SSH key pair if it doesn't exist
generate_ssh_key_pair() {
    if [ ! -f "$PRIVATE_KEY" ]; then
        echo "Generating SSH key pair..."
        mkdir -p ~/.ssh
        ssh-keygen -t rsa -b 2048 -C "ansibleTraining" -f "$PRIVATE_KEY" -N ""
        eval $(ssh-agent)
        ssh-add
        echo -e "${GREEN}SSH key pair generated${NC}"
    else
        echo -e "${YELLOW}SSH key pair already exists${NC}"
    fi
}


check_ssh_configuration(){
    # Check SSH key existence
    if [ ! -e "$PRIVATE_KEY" ]; then
        echo -e "${RED}SSH key not found: $PRIVATE_KEY${NC}"
        exit 1
    fi

    # Check SSH key permissions
    KEY_PERMISSIONS=$(stat -c "%a" "$PRIVATE_KEY")
    if [ "$KEY_PERMISSIONS" != "600" ]; then
        echo -e "${RED}Incorrect permissions for SSH key: $KEY_PERMISSIONS${NC}"
        exit 1
    fi

    # Check SSH agent
    if ! ssh-add -l &>/dev/null; then
        echo -e "${RED}SSH agent is not running or no identities are added.${NC}"
        exit 1
    fi
}

## Functions 
copy_ssh_key() {
    local server_ip=$1
    ssh-copy-id -i $PUBLIC_KEY $USERNAME@$server_ip
    if [ $? -eq 0 ]; then
        echo "${GREEN}SSH key successfully copied to $server_ip${NC}"
    else
        echo "${RED}Failed to copy SSH key to $server_ip${NC}"
    fi
}

## Script
generate_ssh_key_pair
check_ssh_configuration
for server_ip in "${IP_ADDRESSE[@]}"; do
    copy_ssh_key $server_ip
done
