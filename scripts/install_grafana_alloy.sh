#!/bin/bash

# documentation https://grafana.com/docs/alloy/latest/get-started/install/linux/
# alloy logs : sudo journalctl -u alloy
# touch install_grafana_alloy.sh ; sudo chmod +x install_grafana_alloy.sh ; sudo vim install_grafana_alloy.sh


##Variables
# Define color codes
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
NC='\033[0m'

PROMETHEUS_SERVER_IP="34.229.138.216"
GRAFANA_ALLOY_CONFIG_FILE="/etc/alloy/config.alloy"
GRAFANA_ALLOY_TMP_FILE="/tmp/grafana-alloy.yaml.tmp"


## Functions 
isfileexists() {
    if [ -e "$1" ]; then
        echo -e "${GREEN}OK : $1 exists.${NC}"
    else
        echo -e "${RED}KO : $1 doesn't exist.${NC}"
        exit 1
    fi
}

isservicesactive() {
    if systemctl is-active --quiet $1; then
        echo -e "${GREEN}$1 is running.${NC}"
        return 0
    else
        echo -e "${RED}$1 is not running.${NC}"
        return 1
    fi
}

check_ping() {
  local target="$1"
  if ping -c 1 "$target" &> /dev/null; then
      echo -e "${GREEN}OK : Ping to $target was successful.${NC}"
  else
      echo -e "${RED}KO : Ping to $target failed.${NC}"
      return 1
  fi
}

## Script
sudo su << SCRIPT
apt install gpg
mkdir -p /etc/apt/keyrings/
wget -q -O - https://apt.grafana.com/gpg.key | gpg --dearmor | tee /etc/apt/keyrings/grafana.gpg > /dev/null
echo "deb [signed-by=/etc/apt/keyrings/grafana.gpg] https://apt.grafana.com stable main" | tee /etc/apt/sources.list.d/grafana.list
apt-get update
apt-get install alloy

check_ping "$PROMETHEUS_SERVER_IP"
isfileexists "$GRAFANA_ALLOY_CONFIG_FILE"

# Add the new configuration directly to the original file
tee "$GRAFANA_ALLOY_CONFIG_FILE" > /dev/null << EOF
// Sample config for Alloy.
//
// For a full configuration reference, see https://grafana.com/docs/alloy
logging {
  level = "warn"
}

prometheus.exporter.unix "default" {
  include_exporter_metrics = true
  disable_collectors       = ["mdadm"]
}

prometheus.scrape "default" {
  targets    = concat(
       prometheus.exporter.unix.default.targets,
       [{
         // Self-collect metrics
         job         = "alloy",
         __address__ = "54.235.42.27:12345",
       }],
     )
  forward_to = [prometheus.remote_write.default.receiver]
 }

prometheus.remote_write "default" {
       endpoint {
               url = "http://$PROMETHEUS_SERVER_IP:9090/write"
       }
}
EOF

if [ $? -eq 0 ]; then
    echo -e "${GREEN}OK : Configuration file updated.${NC}"
else
    echo -e "${RED}KO : Failed to update configuration file.${NC}"
    exit 1
fi

systemctl enable alloy.service
systemctl start alloy.service
isservicesactive alloy.service
SCRIPT

#check grafana alloy ui
#http://localhost:12345/
#debug : journalctl -n 50 -fu alloy.service