

#metrics-reporter-jmx
#documentation 
#https://github.com/GerritCodeReview/plugins_metrics-reporter-jmx/blob/master/src/main/resources/Documentation/build.md

#repo git clone https://gerrit.googlesource.com/plugins/metrics-reporter-jmx

#install bazel
#doc https://bazel.build/install
yum install bazel

bazel build metrics-reporter-jmx.jar

bazel build libmetrics-reporter-jmx__plugin-src.jar


