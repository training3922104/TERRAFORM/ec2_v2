#!/bin/bash

#aws ec2 describe-instances
#aws ec2 describe-instances --instance-ids <instance-id> --query 'Reservations[*].Instances[*].{Name: Tags[?Key==`Name`].Value | [0], Environment: Tags[?Key==`Environment`].Value | [0]}'
#touch motd.sh ; sudo chmod +x motd.sh ; sudo vim motd.sh

##packages
#sudo apt install -y awscli
#sudo apt install -y jq

##variables

#colors
GREEN="\e[32m"
BLUE="\e[94m"
RED="\e[31m"
YELLOW="\e[33m"
COLOR=""
RESET_COLOR="\e[0m"

#global variables
TERMINAL_WIDTH=$(tput cols)
WIDTH=$(($TERMINAL_WIDTH - 4))
BORDER_CARACTER="-"
BORDER=$(printf "%0.s$BORDER_CARACTER" $(seq 1 $TERMINAL_WIDTH))

#motd data
DOMAIN=$(uname -n)
HOSTNAME=$(curl -s http://169.254.169.254/latest/meta-data/public-hostname)
USER=$(whoami)
OWNER=""
TEAM=""
REGION=$(curl -s http://169.254.169.254/latest/meta-data/placement/region)
ECOSYSTEM=$(uname)
ACCOUNT_ID=$(aws sts get-caller-identity --query "Account" --output text)
COMPONENT=""
INSTANCE=$(echo "$HOSTNAME" | sed 's/^\([^.]*\)\..*/\1/')
INSTANCE_IP=$(curl -s http://169.254.169.254/latest/meta-data/local-ipv4)
INSTANCE_ID=$(curl -s http://169.254.169.254/latest/meta-data/instance-id)
INSTANCE_TYPE=$(curl -s http://169.254.169.254/latest/meta-data/instance-type)
DATACENTER=""
TAGS=$(curl -s http://169.254.169.254/latest/meta-data/tags/instance)
#TODO find a way to retrieve tags
APP=""
APP_VERSION=""
OSRELEASEPATH="/etc/os-release"

# Check if INSTANCE_ID is set
if [ -z "$INSTANCE_ID" ]; then
    echo "INSTANCE_ID is not set"
    exit 1
fi

INSTANCES=$(aws ec2 describe-tags --region $REGION)

# Check if INSTANCES is empty
if [ -z "$INSTANCES" ]; then
    echo "Failed to fetch instance tags"
    exit 1
fi

INSTANCE_NAME=$(echo "$INSTANCES" | jq -r --arg INSTANCE_ID "$INSTANCE_ID" '.Tags[] | select(.ResourceId == $INSTANCE_ID and .Key == "Name") | .Value')
ENVIRONMENT=$(echo "$INSTANCES" | jq -r --arg INSTANCE_ID "$INSTANCE_ID" '.Tags[] | select(.ResourceId == $INSTANCE_ID and .Key == "Environment") | .Value')

# Check if ENVIRONMENT is empty
if [ -z "$ENVIRONMENT" ]; then
    echo "Environment tag not found for INSTANCE_ID: $INSTANCE_ID"
    exit 1
fi

#checks applications
if source /data/venv/ansible/bin/activate && command -v ansible &> /dev/null ; then
    ANSIBLE_DATA=$(source /data/venv/ansible/bin/activate | ansible --version)
    APP="Ansible"
    APP_VERSION=$(echo "$ANSIBLE_DATA" | grep -oP 'core \K\S+' | cut -d']' -f1)
    PYTHON_VERSION=$(echo "$ANSIBLE_DATA" | grep -oP 'python version = \K\S+' | cut -d' ' -f1)
elif command -v puppet &> /dev/null ; then
    APP="Puppet"
    APP_VERSION=$(puppet --version | grep 'puppet' | awk '{print "Puppet version:", $2}')
elif command -v grafana &> /dev/null ; then
    APP="Grafana"
    APP_VERSION=$(puppet --version | grep 'grafana' | awk '{print "Grafana version:", $2}')
elif command -v prometheus &> /dev/null ; then
    APP="Puppet"
    APP_VERSION=$(prometheus --version | grep 'prometheus' | awk '{print "Prometheus version:", $2}')
else 
    APP="Unknown"
    APP_VERSION="N/A"
fi

#check os file
if [ -f "$file_path" ]; then
    OS=$(cat /etc/os-release | grep '^ID=' | cut -d'=' -f2)
    OS_VERSION=$(cat /etc/os-release | grep '^VERSION_ID=' | cut -d'=' -f2)
    OS_SHORTNAME="$OS $OS_VERSION"
    OS_SHORTNAME=$(OS_SHORTNAME//\"/)
else
    echo "File does not exist."
fi
#TODO check seems to have an issue

##functions
#define a color regarding the environment of the machine

color_motd() {
    local ENVIRONMENT=$1
    if [[ $ENVIRONMENT == "Production" ]]; then
        COLOR=$RED
    elif [[ $ENVIRONMENT == "Preproduction" ]]; then
        COLOR=$BLUE
    elif [[ "$ENVIRONMENT" == "Uat" || "$ENVIRONMENT" == "Int" || "$ENVIRONMENT" == "Sandbox" ]]; then
        COLOR=$GREEN
    else
        COLOR="UNKNOWN"
    fi
    echo "$COLOR"
    #TODO define error if no environment is setup
}

# Function to print text inside a continuous line border with centered text
warning() {
    local sentence="$1"
    local text_width=${#text}
    local padding_length=$(( ($TERMINAL_WIDTH - ${#sentence}) / 2 ))

    # Print top border
    printf "$COLOR%s\n$RESET_COLOR" "$BORDER"
    printf "$COLOR%*s%s%*s\n$RESET_COLOR" $padding_length "" "$sentence" $padding_length
    printf "$COLOR%s\n$RESET_COLOR" "$BORDER"
}

print_disclaimer() {

    # Define the text with each sentence on a separate line
    local text=$(printf "$COLOR%sAccess is restricted to authorized users only.\nAll transactions are monitored.\nBy continuing past this point, you expressly consent to this monitoring.%s$RESET_COLOR" )

    # Split the text into an array of lines
    IFS=$'\n' read -r -d '' -a lines <<< "$text"

    for line in "${lines[@]}"; do
        local padding=$(( ($TERMINAL_WIDTH - ${#line}) / 2 ))
        printf "$COLOR%*s%s%s\n$RESET_COLOR" $padding "" "" "$line"
    done
    printf "$COLOR%s\n$RESET_COLOR" "$BORDER"
}

informations() {
    local col1="$1"
    local col2="$2"
    local col3="$3"
    local col4="$4"

    local aside_padding=$(($TERMINAL_WIDTH / 4))
    local total_aside_padding=$((aside_padding * 2))
    local column_width_responsive=$((($TERMINAL_WIDTH - total_aside_padding) / 4 ))
    local column_width=$(($TERMINAL_WIDTH / 4 ))

    if [[ $TERMINAL_WIDTH -gt 190 ]]; then
        printf "%*s" $aside_padding
        printf "$COLOR%*s$RESET_COLOR" $((column_width_responsive)) "$col1"
        printf "%-*s" $((column_width_responsive)) "$col2"
        printf "$COLOR%*s$RESET_COLOR" $((column_width_responsive)) "$col3"
        printf "%-*s" $((column_width_responsive)) "$col4"
        printf "%*s" $aside_padding
        printf "\n" 
    else
        printf "$COLOR%*s$RESET_COLOR" $((column_width)) "$col1"
        printf "%-*s" $((column_width)) "$col2"
        printf "$COLOR%*s$RESET_COLOR" $((column_width)) "$col3"
        printf "%-*s" $((column_width)) "$col4"
        printf "\n"
    fi
}

documentation() {
    local text=$(printf "%ssudo systemctl status service\nsudo systemctl start service\nsudo systemctl restart service\nsudo systemctl stop service%s" )

        # Split the text into an array of lines
    IFS=$'\n' read -r -d '' -a lines <<< "$text"

    # Calculate the maximum line length
    local max_length=0
    for line in "${lines[@]}"; do
        if (( ${#line} > max_length )); then
            max_length=${#line}
        fi
    done

    # Calculate padding for left and right sides
    local padding=$(( ($TERMINAL_WIDTH - max_length) / 2 ))

    echo
    for line in "${lines[@]}"; do
        printf "%*s%s%s\n" $padding "" "$line"
    done
    echo
    printf "$COLOR%s\n$RESET_COLOR" "$BORDER"
}


# MOTD 
COLOR=$(color_motd "$ENVIRONMENT")
warning "! Enterprise property !"
print_disclaimer
echo
echo "$COLOR Informations: $RESET_COLOR"
printf "$YELLOW%*s%s%s\n$RESET_COLOR" "$(( ($TERMINAL_WIDTH - ${#DOMAIN}) / 2 ))" "" "$DOMAIN"
echo
informations "Ecosystem: " "$ECOSYSTEM" "Owner: " "$OWNER"
informations "Os: " "$OS_SHORTNAME" "Instance name: " "$INSTANCE_NAME"
informations "User: " "$USER" "Instance aws: " "${DOMAIN%%.vnet*}"
informations "Team: " "$TEAM" "Instance id: " "$INSTANCE_ID"
informations "Region: " "$REGION" "Instance ip: " "$INSTANCE_IP"
informations "" "" "Account_id: " "$ACCOUNT_ID"
informations "$APP: " "$APP_VERSION" "Environment: " "$ENVIRONMENT"
if [[ -n $PYTHON_VERSION && -v PYTHON_VERSION ]]; then
    informations "Python: " "$PYTHON_VERSION" "" ""
fi
echo
echo "$COLOR Documentation: $RESET_COLOR"
echo
documentation
