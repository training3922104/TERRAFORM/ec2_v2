#!/opt/collabnet/teamforge/runtime/bin/wrapper/sh

#location : /opt/collabnet/teamforge/runtime/conf/set-env.sh

eval "`/opt/collabnet/teamforge/runtime/scripts/options-to-shell-variables.py`"

SOURCEFORGE_HOME="/opt/collabnet/teamforge/runtime/sourceforge_home"
JAVA_HOME="/usr/lib/jvm/java-11-openjdk"
JAVA_OPTS="-Xms2048m -Xmx6144m -XX:+UseParallelGC -XX:MaxMetaspaceSize=512m -XX:ReservedCodeCacheSize=128M -server -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/tmp -verbose:gc -XX:+PrintCodeCache -Dsun.rmi.dgc.client.gcInterval=600000 -Dsun.rmi.dgc.server.gcInterval=600000 -Djava.security.egd=file:/dev/urandom -Djava.awt.headless=true -Dsourceforge.home=/opt/collabnet/teamforge/runtime/sourceforge_home -Dsourceforge.app.logdir=/opt/collabnet/teamforge/log/apps -Dapp.data=/opt/collabnet/teamforge/var -Dapp.runtime=/opt/collabnet/teamforge/runtime -Dapp.distribution=/opt/collabnet/teamforge/dist -Dapp.etc=/opt/collabnet/teamforge/etc -Dlogging.configuration=file:////opt/collabnet/teamforge/runtime/jboss/standalone/configuration/logging.properties -Djava.io.tmpdir=/opt/collabnet/teamforge/runtime/temp -Xlog:gc* -javaagent:/data/jmx/jmx_prometheus_javaagent-0.18.0.jar=9130:/data/jmx/jboss.yml -Djava.util.logging.manager=org.jboss.logmanager.LogManager -Djboss.modules.system.pkgs=org.jboss.logmanager -Dsun.util.logging.disableCallerCheck=true -Xbootclasspath/a:$JBOSS_HOME/modules/system/layers/base/org/jboss/logmanager/main/jboss-logmanager-2.1.17.Final.jar:$JBOSS_HOME/modules/system/layers/base/org/wildfly/common/main/wildfly-common-1.5.4.Final.jar:$JBOSS_HOME/modules/system/layers/base/org/jboss/log4j/logmanager/main/log4j-jboss-logmanager-1.2.0.Final.jar"
INTEGRATION_JVM_OPTS="-Xms160m -Xmx160m -server -XX:+HeapDumpOnOutOfMemoryError  -XX:HeapDumpPath=/tmp -verbose:gc -Dsun.rmi.dgc.client.gcInterval=600000 -Dsun.rmi.dgc.server.gcInterval=600000 -Djava.security.egd=file:/dev/urandom -XX:+UseParallelGC -Djava.awt.headless=true -Dsourceforge.home=/opt/collabnet/teamforge/runtime/sourceforge_home -Dsourceforge.logdir=/opt/collabnet/teamforge/log/integration -Dapp.data=/opt/collabnet/teamforge/var -Dapp.runtime=/opt/collabnet/teamforge/runtime -Dapp.distribution=/opt/collabnet/teamforge/dist -Dapp.log=/opt/collabnet/teamforge/log -Djsse.enableSNIExtension=false -Xlog:gc* -javaagent:/data/jmx/jmx_prometheus_javaagent-0.18.0.jar=9131:/data/jmx/config.yaml"
PHOENIX_JVM_OPTS="-Xms256m -Xmx256m -server -XX:+HeapDumpOnOutOfMemoryError  -XX:HeapDumpPath=/tmp -verbose:gc -Dsun.rmi.dgc.client.gcInterval=600000 -Dsun.rmi.dgc.server.gcInterval=600000 -Djava.security.egd=file:/dev/urandom -XX:+UseParallelGC -Dapp.runtime=/opt/collabnet/teamforge/runtime -Dsourceforge.home=/opt/collabnet/teamforge/runtime/sourceforge_home -Dapp.distribution=/opt/collabnet/teamforge/dist -Xlog:gc*"
PATH="$PATH:$JAVA_HOME/bin:/usr/sbin/"

ETL_JVM_OPTS="-Xms160m -Xmx512m -Djavax.net.ssl.trustStore=/usr/java/java-11-openjdk/lib/security/cacerts -Djavax.net.ssl.trustStoreType=JKS -server -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/tmp -verbose:gc -Dsun.rmi.dgc.client.gcInterval=600000 -Dsun.rmi.dgc.server.gcInterval=600000 -Djava.security.egd=file:/dev/urandom -Djava.awt.headless=true -Dsourceforge.home=/opt/collabnet/teamforge/runtime/sourceforge_home -Dsourceforge.logdir=/opt/collabnet/teamforge/log/etl -Dapp.data=/opt/collabnet/teamforge/var -Dapp.runtime=/opt/collabnet/teamforge/runtime -Dapp.distribution=/opt/collabnet/teamforge/dist -Dapp.log=/opt/collabnet/teamforge/log -Xlog:gc*"

if [ "X{__TEST_SUPPORT_ENABLED__}" = "Xtrue" ]; then
    JAVA_OPTS="$JAVA_OPTS -Dsf.realScmMode=true -Dsf.messagingDebugMode=true"
    INTEGRATION_JVM_OPTS="$INTEGRATION_JVM_OPTS -Dsf.realScmMode=true"
fi

export JAVA_HOME JAVA_OPTS PHOENIX_JVM_OPTS INTEGRATION_JVM_OPTS JBOSS_HOME PATH JBOSSSH

if [ -n "$UMASK" ]; then
    umask $UMASK
fi
~
