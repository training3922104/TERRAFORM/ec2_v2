
variable "security_id_server" {
  description = "Security Group ID for Server instances"
}

variable "security_id_prometheus" {
  description = "Security Group ID for Prometheus instances"
}

variable "security_id_grafana" {
  description = "Security Group ID for Grafana instances"
}

variable "security_id_grafanaalloy" {
  description = "Security Group ID for Grafana alloy instance"
}

variable "security_id_haproxy" {
  description = "Security Group ID for Haproxy instances"
}

variable "security_id_syslog" {
  description = "Security Group ID for Syslog instances"
}

variable "security_id_postgres" {
  description = "Security Group ID for postgres instances"
}

variable "security_id_pgwatch2" {
  description = "Security Group ID for pgwatch2 instances"
}

variable "grafana_instance_profile" {
  description = "grafana_instance_profile"
}

variable "prometheus_instance_profile" {
  description = "prometheus_instance_profile"
}

